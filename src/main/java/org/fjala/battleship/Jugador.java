package org.fjala.battleship;

import java.util.Scanner;

class Jugador {
    Scanner entrada = new Scanner(System.in);
    protected int barcos_destruidos;
    protected int barcos_restantes;
    protected int puntaje;
    private String jugador1;
    private String jugador2;
    private String administrador;
    private String contraseniaGabriel = "123";
    private String contraseniaTomas = "456";
    private String contraseniaAlejandro = "789";
    private String contraseniaJorge = "012";
    private String contraseniaJhonatan = "321";
    private String ingresoDatos;

    public void crearJugador() {
        int contador = 0;
        while (contador < 2) {
            String eleccion = "";
            System.out.println("¿Como ingresaras?\n1.- Como Jugador\n2.- Como Administrador");
            while (!(eleccion.equals("1")) && !(eleccion.equals("2"))) {
                eleccion = entrada.nextLine();
                if (eleccion.equals("1")) {
                    if (contador == 0) {
                        System.out.println("¿Jugador nº1 con que apodo quieres empezar?");
                        jugador1 = entrada.nextLine();
                    } else {
                        System.out.println("¿Jugador nº2 con que apodo quieres empezar?");
                        jugador2 = entrada.nextLine();
                    }
                } else if (eleccion.equals("2")) {
                    //ingresarAdministrador();
                    if (contador == 0) {
                        jugador1 = ingresarAdministrador();
                    } else {
                        jugador2 = ingresarAdministrador();
                    }
                } else {
                    System.out.println("Por favor escoja una de las opciones que se le da: ");
                }
            }
            contador++;
        }
        System.out.println("---------- Empieza el duelo entre " + jugador1 + " y " + jugador2 + " ---------------");
    }

    public String ingresarAdministrador() {
        boolean controlador = false;

        while (controlador == false) {
            System.out.println("Ingrese su contraseña para empezar: ");
            ingresoDatos = entrada.nextLine();
            if (contraseniaGabriel.equals(ingresoDatos)) {
                administrador = "Gabriel";
                controlador = true;
            } else if (contraseniaTomas.equals(ingresoDatos)) {
                administrador = "Tomas";
                controlador = true;
            } else if (contraseniaAlejandro.equals(ingresoDatos)) {
                administrador = "Alejandro";
                controlador = true;
            } else if (contraseniaJorge.equals(ingresoDatos)) {
                administrador = "Jorge";
                controlador = true;
            } else if (contraseniaJhonatan.equals(ingresoDatos)) {
                administrador = "Jhonatan";
                controlador = true;
            } else {
                System.out.println("Contraseña incorrecta\nPor favor vuelva a intentar: ");
                controlador = false;
            }
        }
        return administrador;
    }

    public void caracteristicas() {
        //barcos_destruidos = aqui deberia venir un metodo que me devuelva el valor de los barcos destruidos y restantes
        System.out.println("Nº de barcos destruidos: " + barcos_destruidos);
        System.out.println("Nº de barcos restantes: " + barcos_restantes);
    }
}