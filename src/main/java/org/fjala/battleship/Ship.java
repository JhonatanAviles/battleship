package org.fjala.battleship;

/**
 * Ship
 */
public class Ship {

    protected boolean sunken=false;   
    protected int hp;
    protected int size;
    protected int xStart;
    protected int xEnd;
    protected int yStart;
    protected int yEnd;
    protected boolean isVertical;

    public Ship(int hp, int size){
        this.hp=hp;
        this.size=size;
    }
    public void takeDamage(){
        hp=hp-1;
        if(hp==0){
            sunken = true;
        }
    }
    public void showHealth(){
        System.out.println("la vida restante es: "+hp);
    }
}


