package org.fjala.battleship;

import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        Inicio juego = new Inicio();
        juego.inicioJuego();
        //Jugador jugador = new Jugador();
        //jugador.crearJugador();
        Scanner scanner = new Scanner(System.in);
        BoardMatrix BoardMatrix = new BoardMatrix();
        BoardMatrix BoardMatrix2 = new BoardMatrix();
        BoardMatrix.createBoard();
        System.out.println("-------------------------------------------------------------------------------------");
        BoardMatrix2.createBoard();
        while(BoardMatrix.sunkenShipCounter<=11||BoardMatrix2.sunkenShipCounter<=11){
            System.out.println("el formato es, ej: (02,02)(10,10)(12,12)(13,05)");
            System.out.println(" jugador 2 ponga la coordenada (x,y) donde desea atacar");
            String str = scanner.next();
            System.out.println("jugador 1 ponga las coordenadas (x,y) donde desea atacar");
            String str2 = scanner.next();
            BoardMatrix.attack(str);
            System.out.println("el numero de barcos hundidos es; "+BoardMatrix.sunkenShipCounter);
            System.out.println("-------------------------------------");
            BoardMatrix.attack(str2);
            System.out.println("el numero de barcos hundidos es; "+BoardMatrix.sunkenShipCounter);
        }
        scanner.close();
        //las siguientes lineas era para la interfaz grafica, pero por temas de tiempo nose completo,
        // para porbar la interfaz se debe comentar toda la parte superior.
        InterfazGrafica i1 = new InterfazGrafica();
        i1.setVisible(true); //hacemos visible la interfaz grafica 
    }
}