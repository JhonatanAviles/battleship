package org.fjala.battleship;

import java.util.Scanner;

class Inicio {
    //boolean inicioFin = true;
    private int dificultad;
    private int desicion;
    private Jugador jugadores = new Jugador();

    void inicioJuego() {
        Scanner entrada = new Scanner(System.in);
        while (desicion != 1 && desicion != 2) {
            System.out.println("Battleship\n1.- Iniciar Juego\n2.- Salir");
            desicion = entrada.nextInt();
            if (desicion == 1) {
                while (dificultad != 1 && dificultad != 2) {
                    System.out.println("Elija una dificultad\n1.- Facil\n2.- Dificil");
                    dificultad = entrada.nextInt();
                    if (dificultad == 1) {
                        System.out.println("Eligio facil");
                        jugadores.crearJugador();
                    } else if (dificultad == 2) {
                        System.out.println("Eligio dificil");
                        jugadores.crearJugador();
                    } else {
                        System.out.println("Elija una de las opciones que se le da porfavor: ");
                    }
                }
            } else if (desicion == 2) {
                System.out.println("Vuelve cuando quieras");
            } else {
                System.out.println("Elija una de las opciones que se le da porfavor: ");
            }
        }

    }
}