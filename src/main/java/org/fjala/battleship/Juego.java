package org.fjala.battleship;
/**
 * Juego
 */
import java.util.Scanner;
public class Juego {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        BoardMatrix BoardMatrix = new BoardMatrix();
        BoardMatrix BoardMatrix2 = new BoardMatrix();
        BoardMatrix.createBoard();
        System.out.println("-------------------------------------------------------------------------------------");
        BoardMatrix2.createBoard();
        while(BoardMatrix.sunkenShipCounter<=11||BoardMatrix2.sunkenShipCounter<=11){
            System.out.println(" jugador 2 ponga la coordenada (x,y) donde desea atacar");
            String str = scanner.next();
            System.out.println("jugador 1 ponga las coordenadas (x,y) donde desea atacar");
            String str2 = scanner.next();
            BoardMatrix.attack(str);
            System.out.println("el numero de barcos hundidos es; "+BoardMatrix.sunkenShipCounter);
            System.out.println("-------------------------------------");
            BoardMatrix.attack(str2);
            System.out.println("el numero de barcos hundidos es; "+BoardMatrix.sunkenShipCounter);
        }
        scanner.close();
    }
}