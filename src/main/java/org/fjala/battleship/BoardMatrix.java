package org.fjala.battleship;

import java.util.ArrayList;
import java.util.List;

public class BoardMatrix {
    private Ship[][] boardMatrix = new Ship[20][20];
    private String[][] charactersBoardMatrix = new String[20][20];
    private List<Ship> placedShips = new ArrayList<>();
    protected int sunkenShipCounter=0;

    BoardMatrix() {
    }

    // --------------------------
    // board creation
    // --------------------------
    public void createBoard() {
        // get unplaced ships
        List<Ship> unplacedShips = getUnplacedShips();

        // for each ship place it on board
        for (Ship unplacedShip : unplacedShips) {
            while (true) {
                // get start coordinates
                int x = (int) (Math.random() * 20);
                int y = (int) (Math.random() * 20);

                // check in matrix
                if (this.isMatrixSpaceAvailable(x, y, unplacedShip.size)) {
                    // set ship attributes
                    this.setShipAttributes(x, y, unplacedShip);

                    // check collisions with placed ships
                    if (!this.isColliding(unplacedShip)) {
                        this.addNewShipToBoard(unplacedShip);
                        break;
                    }
                }
            }
        }

        this.showMatrixToAdmi();
    }

    public List<Ship> getUnplacedShips() {
        List<Ship> unplacedShips = new ArrayList<>();
        unplacedShips.add(new PortaAviones());
        unplacedShips.add(new Crucero());
        unplacedShips.add(new Destructor());
        unplacedShips.add(new Destructor());
        unplacedShips.add(new Acorazado());
        unplacedShips.add(new Acorazado());
        unplacedShips.add(new Acorazado());
        unplacedShips.add(new Fragata());
        unplacedShips.add(new Fragata());
        unplacedShips.add(new Fragata());
        unplacedShips.add(new Fragata());
        return unplacedShips;
    }

    private boolean isMatrixSpaceAvailable(int x, int y, int size) {
        return boardMatrix[y][x] == null &&
                (x + size - 1 <= 19 && y + size - 1 <= 19);
    }

    private void setShipAttributes(int x, int y, Ship ship) {
        ship.xStart = x;
        ship.yStart = y;
        ship.isVertical = ((int) (Math.random() * 2) == 1);

        if (ship.isVertical) {
            ship.yEnd = y + ship.size - 1;
        } else {
            ship.xEnd = x + ship.size - 1;
        }
    }

    private void addNewShipToBoard(Ship unplacedShip) {
        int x = unplacedShip.xStart;
        int y = unplacedShip.yStart;

        placedShips.add(unplacedShip);
        boardMatrix[y][x] = unplacedShip;
        for (int i = 0; i < unplacedShip.size - 1; i++) {
            if (!unplacedShip.isVertical) {
                x++;
            } else {
                y++;
            }

            boardMatrix[y][x] = unplacedShip;
        }
    }

    private boolean isColliding(Ship newShip) {
        if (placedShips != null) {
            for (Ship ship : placedShips) {
                if (newShip.isVertical) {
                    if (ship.isVertical) {
                        return this.isVerticalCollision(newShip, ship);
                    } else {
                        return this.isCrossedCollision(newShip, ship);
                    }
                } else {
                    if (!ship.isVertical) {
                        return this.isHorizontalCollision(newShip, ship);
                    } else {
                        return this.isCrossedCollision(newShip, ship);
                    }
                }
            }

        }
        return false;
    }

    private boolean isVerticalCollision(Ship newShip, Ship ship) {
        return (newShip.yStart <= ship.yEnd && newShip.yStart >= ship.yStart) ||
                (newShip.yEnd <= ship.yEnd && newShip.yEnd >= ship.yStart) ||
                (newShip.yStart <= ship.yStart && newShip.yEnd >= ship.yEnd);
    }

    private boolean isHorizontalCollision(Ship newShip, Ship ship) {
        return (newShip.xStart <= ship.xEnd && newShip.xStart >= ship.xStart) ||
                (newShip.xEnd <= ship.xEnd && newShip.xEnd >= ship.xStart) ||
                (newShip.xStart <= ship.xStart && newShip.xEnd >= ship.xEnd);
    }

    private boolean isCrossedCollision(Ship newShip, Ship ship) {
        return (newShip.xStart <= ship.xStart && newShip.xEnd >= ship.xEnd &&
                newShip.yStart >= ship.yStart && newShip.yEnd <= ship.yEnd) ||
                (newShip.xStart >= ship.xStart && newShip.xEnd <= ship.xEnd &&
                        newShip.yStart <= ship.yStart && newShip.yEnd >= ship.yEnd);
    }

    // --------------------------
    // matrix actions
    // --------------------------
    public void showMatrixToAdmi() {
        for (int i = 0; i < 20; i++) {
            for (int j = 0; j < 20; j++) {
                if (boardMatrix[i][j] instanceof PortaAviones) {
                    charactersBoardMatrix[i][j] = "[$]";
                } else if (boardMatrix[i][j] instanceof Crucero) {
                    charactersBoardMatrix[i][j] = "[@]";
                } else if (boardMatrix[i][j] instanceof Destructor) {
                    charactersBoardMatrix[i][j] = "[#]";
                } else if (boardMatrix[i][j] instanceof Acorazado) {
                    charactersBoardMatrix[i][j] = "[%]";
                } else if (boardMatrix[i][j] instanceof Fragata) {
                    charactersBoardMatrix[i][j] = "[&]";
                } else {
                    charactersBoardMatrix[i][j] = "[ ]";
                }
                System.out.print(charactersBoardMatrix[i][j]);
            }
            System.out.println();
        }
    }
    public void showMatrixToPlayer() {
        for (int i = 0; i < 20; i++) {
            for (int j = 0; j < 20; j++) {
                charactersBoardMatrix[i][j] = "[ ]";
                System.out.print(charactersBoardMatrix[i][j]);
            }
            System.out.println();
        }
    }


    public void attack(String str) {
        String str1 = str.substring(1,3);
        String str2 = str.substring(4,6);
        String str3 = str.substring(8,10);
        String str4 = str.substring(11,13);
        String str5 = str.substring(15,17);
        String str6 = str.substring(18,20);
        String str7 = str.substring(22,24);
        String str8 = str.substring(25,27);
        String [] coordinate= {str1,str2,str3,str4,str5,str6,str7,str8};
        for (int k=0;k<8;k=k+2) {
            int x = Integer.parseInt(coordinate[k]);
            int y = Integer.parseInt(coordinate[k+1]);
            if (boardMatrix[y][x] != null) {
                boardMatrix[y][x].takeDamage();
                charactersBoardMatrix[y][x] = "[-]";
                if(boardMatrix[y][x].sunken){
                    sunkenShipCounter++;
                }
            } else{
                charactersBoardMatrix[y][x]="[X]";
            }
        }
        for (int i = 0; i < 20; i++) {
            for (int j = 0; j < 20; j++) {
                System.out.print(charactersBoardMatrix[i][j]);
            }
            System.out.println();
        }
        
    }
}

