package org.fjala.battleship;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

class InterfazGrafica extends JFrame {
    public JPanel panel; // instanciamos el panel
    JLabel etiqueta;
    JLabel etiqueta2;
    JButton boton1;
    JButton boton2;
    String nombreBoton2 = "";

    public InterfazGrafica() {
        setTitle("Batalla de Barcos"); //le ponemos un titulo al juego
        setSize(500, 500); //ponemos el tamaño de la interfaz grafica primero ancho y segundo largo
        setLocationRelativeTo(null); //es para poner la interfaz grafica en el medio
        iniciarComponentes();

        setDefaultCloseOperation(EXIT_ON_CLOSE); // hacemos que el juego al apretar x tambien termine el programa
    }

    private void iniciarComponentes() {
        colocarPanel();
        colocarEtiqueta();
        colocarEtiquetaImagen();
        colocarBotones("Jugar", 125, 400, 'a', "Salir", 250, 400, 's');
        //eventoOyenteDeAccion();
        //colocarBotones("Salir", 250, 400, 's');
        //botonSalir();
    }

    private void colocarPanel() {
        panel = new JPanel(); //creando el panel
        panel.setLayout(null); //desactivamos el diseño de donde se ponen las etiquetas en el panel
        panel.setBackground(Color.RED); //le damos un color al panel
        this.getContentPane().add(panel); //ponemos el panel en sima de la interfaz grafica ¿Preguntar a Cristelh porque funciona con el this
    }

    private void colocarEtiqueta() {
        etiqueta = new JLabel("Battleship"); //creamos una etiqueta osea el titulo battleship
        etiqueta.setBounds(180, 10, 300, 80); //establecemos la posicion donde aparecera la etiqueta x,y le ponemos el ancho y despues el largo
        etiqueta.setForeground(Color.BLUE); // le agregamos color a la etiqueta
        etiqueta.setFont(new Font("arial", 3, 30)); // ponemos el tipo de letra, estilo de tipo de letra, tamaño de la letra
        panel.add(etiqueta); //agregamos la etiqueta al panel 

        /*ImageIcon imagen = new ImageIcon("Battleship.jpeg"); //ponemos la imagen 
        etiqueta2 = new JLabel(); // creamos otra etiqueta donde pondremos ahora una imagen
        etiqueta2.setBounds(100, 80, 300, 300); //ponemos posicion ancho y alto de la etiqueta no de la imagen
        etiqueta2.setIcon(new ImageIcon(imagen.getImage().getScaledInstance(etiqueta2.getWidth(), etiqueta2.getHeight(), Image.SCALE_SMOOTH))); //introducimos la imagen en etiqueta 2 y hacemos que la imagen se achique (primero ancho de la imagen, largo de la imagen, forma escalado)
        panel.add(etiqueta2); // agregamos etiqueta2 al panel*/
    }

    private void colocarEtiquetaImagen() {
        ImageIcon imagen = new ImageIcon("Battleship.jpeg"); //ponemos la imagen 
        etiqueta2 = new JLabel(); // creamos otra etiqueta donde pondremos ahora una imagen
        etiqueta2.setBounds(100, 80, 300, 300); //ponemos posicion ancho y alto de la etiqueta no de la imagen
        etiqueta2.setIcon(new ImageIcon(imagen.getImage().getScaledInstance(etiqueta2.getWidth(), etiqueta2.getHeight(), Image.SCALE_SMOOTH))); //introducimos la imagen en etiqueta 2 y hacemos que la imagen se achique (primero ancho de la imagen, largo de la imagen, forma escalado)
        panel.add(etiqueta2); // agregamos etiqueta2 al panel
    }

    private void colocarBotones(String nombreBoton, int boton1EjeX, int boton1EjeY, char teclaBoton, String ingresoNombreBoton2, int boton2EjeX, int boton2EjeY, char teclaBoton2) {
        boton1 = new JButton(nombreBoton); // creamos el boton y le añadimos el constructor para añadir texto al boton
        boton1.setBounds(boton1EjeX, boton1EjeY, 100, 40); //ponemos la posicion en que estara el boton en el panel, el ancho y el largo
        boton1.setEnabled(true); //(no es necesario si vamos a interactuar con el boton todo el tiempo) aqui damos autorizacion para poder interactuar con el boton si esta en false ya no se puede interactuar con el boton 
        boton1.setMnemonic(teclaBoton); //agregamos un char y podemos interactuar con el boton apretando alt + lo que pongamos en el char
        boton1.setForeground(Color.BLUE); //establecemos el color de la letra del boton
        boton1.setFont(new Font("cooper black", 3, 20)); //Establecemos tipo de letra, estilo de letra, tamaño de letra
        panel.add(boton1); // añadimos el boton al panel

        eventoOyenteDeAccion();

        nombreBoton2 = ingresoNombreBoton2;
        boton2 = new JButton(nombreBoton2); // creamos el boton y le añadimos el constructor para añadir texto al boton
        boton2.setBounds(boton2EjeX, boton2EjeY, 100, 40); //ponemos la posicion en que estara el boton en el panel, el ancho y el largo
        boton2.setEnabled(true); //(no es necesario si vamos a interactuar con el boton todo el tiempo) aqui damos autorizacion para poder interactuar con el boton si esta en false ya no se puede interactuar con el boton 
        boton2.setMnemonic(teclaBoton2); //agregamos un char y podemos interactuar con el boton apretando alt + lo que pongamos en el char
        boton2.setForeground(Color.BLUE); //establecemos el color de la letra del boton
        boton2.setFont(new Font("cooper black", 3, 20)); //Establecemos tipo de letra, estilo de letra, tamaño de letra
        panel.add(boton2); // añadimos el boton al panel

        if (nombreBoton2.equalsIgnoreCase("Salir")) {
            botonSalir();
        } else {

        }
        /*
        //boton 2 con imagen
        JButton boton2 = new JButton(); //creamos el boton
        boton2.setBounds(250, 100, 100, 40); //ponemos la posicion en la que estara en el panel, ancho y largo del boton
        ImageIcon click = new ImageIcon("boton.jpeg"); // agregamos la imagen
        boton2.setIcon(new ImageIcon(click.getImage().getScaledInstance(boton2.getWidth(), boton2.getHeight(), Image.SCALE_SMOOTH))); //configuramos el tamaño de la imagen su ancho, largo, y su escalado
        boton2.setBackground(Color.GREEN); //establecemos color de fondo del boton
        panel.add(boton2); //añadimos boton2 al panel*/
    }

    private void eventoOyenteDeAccion() {
        ActionListener oyenteDeAccion = new ActionListener() { //nos creamos un objeto de ActionListener porque en el add nos pide eso

            @Override //implementando todos los metodos abstractos de esa interface
            public void actionPerformed(ActionEvent ae) {
                //colocarPanel();
                etiqueta.setText("Elija una dificultad"); //renombramos la etiqueta
                etiqueta.setBounds(10, 10, 300, 80); //lo posicionamos en el panel x,y despues agregamos el ancho y el largo
                etiqueta2.setIcon(null); //quitamos la imagen
                panel.remove(boton1); //removemos el boton1 del panel pero nose porque no funciona con los 2
                //panel.remove(colocarBotones(nombreBoton, boton1EjeX, boton1EjeY, teclaBoton, ingresoNombreBoton2, boton2EjeX, boton2EjeY, teclaBoton2););
                colocarBotones("Facil", 175, 100, 'z', "Dificil", 175, 200, 'x'); //creamos lo nuevos botones
                //boton.setBounds(null);
                //colocarBotones("Facil", 175, 100, 'z');
                //colocarBotones("Dificil", 175, 200, 'x');
            }
        };

        boton1.addActionListener(oyenteDeAccion); //agregamos un oyente de accion cuando agamos click en boton1 pasara algo

        /*ActionListener oyenteDeAccion2 = new ActionListener(){ //nos creamos un objeto de ActionListener porque en el add nos pide eso

            @Override //implementando todos los metodos abstractos de esa interface
            public void actionPerformed(ActionEvent ae){
                dispose();
            } 
        };

        boton2.addActionListener(oyenteDeAccion2); //agregamos un oyente de accion cuando agamos click en boton2 pasara algo*/


    }

    private void botonSalir() {

        ActionListener oyenteDeAccion2 = new ActionListener() { //nos creamos un objeto de ActionListener porque en el add nos pide eso

            @Override //implementando todos los metodos abstractos de esa interface
            public void actionPerformed(ActionEvent ae) {
                dispose(); // metodo para cuando apretamos el boton salga
            }
        };

        boton2.addActionListener(oyenteDeAccion2); //agregamos un oyente de accion cuando agamos click en boton2 pasara algo
    }

}