package org.fjala.battleship;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class TestShip {

    static Ship ship;

    @BeforeAll
    static void init(){
        ship = new Ship(4,4);
    }

    @Test
    void testHit(){
        int expected = 3;
        int actual;
        ship.takeDamage();
        actual = ship.hp;
        assertEquals(expected,actual);
    }

    @Test
    void testSinkShip(){
        for(int i = 0; i < 3; i++){
            ship.takeDamage();
        }
        assertThat(ship.sunken,is(true));
    }

}
